.SUFFIXES:
.SECONDARY:

PIC=PIC12F675
#SDCC_VERBOSE=-V
SDCCPATH=/0/Aplicaciones/sdcc-3.6.0
CFLAGS=-S $(SDCC_VERBOSE) --use-non-free -mpic14 -p12f675
NONFREE_LIBPATH=$(SDCCPATH)/share/sdcc/non-free/lib
PIC12F675DEV=$(NONFREE_LIBPATH)/pic14/pic12f675.lib
LIBSDCC=$(SDCCPATH)/share/sdcc/lib/pic14/libsdcc.lib
LKR=$(SDCCPATH)/gputils/share/gputils/lkr/12f675_g.lkr
OBJECT=$(patsubst %.c,%.hex,$(wildcard *.c))
TOCLEAN=$(wildcard *.o *.cod *.asm *.hex)

all: $(OBJECT)

%.asm: %.c
	$Qsdcc $(CFLAGS) $<

%.o: %.asm
	$Qgpasm -c -w1 -o $@ $<

%.hex: %.o
	$Qgplink -q -s $(LKR) -C -l -w -r -o $@ $? $(PIC12F675DEV) $(LIBSDCC) $(LIBIO) && size $@

upload: $(OBJECT)
	$Qpk2cmd -P$(PIC) -R -T -M -F $<

readosccal:
	$Qpk2cmd -P$(PIC) -GP0x3FF-0x3FF

readeeprom:
	$Qpk2cmd -P$(PIC) -GE0x0-0x0F

showprogram:
	$Qpk2cmd -P$(PIC) -GE0x0-0xFF | gawk '/^[0-9]?{4} / { for(n = 2; n < 10; n++) if($$n != "FF") printf("$Pc", strtonum(sprintf("0x$Ps", $$n))); } END { print "" }'

clean:
	$Q$(RM) $(TOCLEAN)

include ../pp.mk
