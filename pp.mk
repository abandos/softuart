# Pretty print of compiler messages: V=0..2
V?=1
ifeq ($V,2)
	# Full verbose
	Q=
	R=$1 $2
else ifeq ($V,1) # default
	# Pretty short message
	msg=@printf "\e[32m\e[1m%7.7s\e[0m $2\n" "$1";
	Q=$(call msg,$(if $<,$(patsubst %.c,CC,$(patsubst %.asm,AS,$(patsubst %.o,LINK,$(patsubst %.hex,UPLOAD,$<)))),$(shell A=$(MAKECMDGOALS); echo $${A^^})),$(if $(findstring clean,$@),$(TOCLEAN),$<))
	R=$(call msg,$(shell A=$1; echo $${A^^}),$2) $1 $2
else # V=0, forced very quiet
	# Full quiet
	Q=@
	R=@$(shell $1 $2)
endif
